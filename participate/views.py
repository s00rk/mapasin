from django.shortcuts import render, redirect
from django.template.response import TemplateResponse 
from django.views.generic import TemplateView
from django.core.paginator import Paginator
from datetime import datetime

from .models import Event

from app.views import getCategories

class EventsView(TemplateView):
	template_name = 'event_list.html'

	def get(self, request, *args, **kwargs):
		paginator = Paginator(Event.objects.order_by('-date'), 25)

		page = request.GET.get('page')
		events = paginator.get_page(page)

		current = events.number

		pages = []
		for p in paginator.page_range:
			if p == (current-2) or p == (current-1) or p == current or p == (current+1) or p == (current+2):
				pages.append(p)

		calendar = Event.objects.order_by('-date')[:100]
		currents = Event.objects.filter(date__month=datetime.today().month)
		return render(request, self.template_name, { 'events': events, 'categories': getCategories(), 'pages': pages, 'calendar': calendar, 'currents': currents })


class EventView(TemplateView):
	template_name = 'event.html'

	def get(self, request, slug, *args, **kwargs):
		event = Event.objects.filter(slug=slug).first()

		return render(request, self.template_name, { 'event': event, 'categories': getCategories() })
