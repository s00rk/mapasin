from django.db import models
from django.utils.text import slugify
import itertools

class Event(models.Model):
	title = models.CharField(max_length=200, verbose_name='Titulo')
	slug = models.SlugField(max_length=300, unique=True, editable=False)
	date = models.DateTimeField(verbose_name='Fecha')
	place = models.CharField(max_length=300, verbose_name='Lugar')
	content = models.TextField(verbose_name='Descripción')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Evento'
		verbose_name_plural = 'Eventos'

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(str(self.date) + '-' + self.title)[:300]
			for x in itertools.count(1):
				if not Event.objects.filter(slug=self.slug).exists():
					break
				self.slug = slugify("%s-%s-%d" % (str(self.date), self.title[:300 - len(str(x)) - 1], x))
		super().save(*args, **kwargs)

	def __str__(self):
		return self.title

class ServiceSocial(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	email = models.EmailField(max_length=200, verbose_name='Correo electrónico')
	career = models.CharField(max_length=200, verbose_name='Carrera')
	work = models.TextField(verbose_name='¿Cómo puedes ayudarnos?')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Servicio Social'
		verbose_name_plural = 'Servicio Social'

	def __str__(self):
		return self.name

class IdeaCategory(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Categoria Idea'
		verbose_name_plural = 'Categorias Ideas'

	def __str__(self):
		return self.name


class Idea(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	email = models.EmailField(max_length=200, verbose_name='Correo electrónico')
	idea = models.TextField(verbose_name='Idea')
	file = models.FileField(upload_to='idea/', verbose_name='Archivo', blank=True, null=True)
	category = models.ForeignKey(IdeaCategory, on_delete=models.SET_NULL, verbose_name='Categoria Idea', blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Idea'
		verbose_name_plural = 'Ideas'

	def __str__(self):
		return self.name

class Subscriber(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	email = models.EmailField(max_length=200, verbose_name='Correo electrónico')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Suscriptor'
		verbose_name_plural = 'Suscriptores'

	def __str__(self):
		return self.name
