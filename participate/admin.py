from django import forms
from django.db import models
from django.contrib import admin

import csv
from django.http import HttpResponse

from .models import Subscriber, IdeaCategory, Idea, ServiceSocial, Event

class ExportCsvMixin:
	def export_as_csv(self, request, queryset):

		meta = self.model._meta
		field_names = [field.name for field in meta.fields]

		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
		writer = csv.writer(response)

		writer.writerow(field_names)
		for obj in queryset:
			row = writer.writerow([getattr(obj, field) for field in field_names])

		return response

	export_as_csv.short_description = "Exportar Seleccionados"

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	list_display = ('title', 'place', 'date')
	exclude = ['slug']
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

	class Media:
		js = ('ckeditor/ckeditor.js', 'ckeditor/margin.js')

@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin, ExportCsvMixin):
	list_display = ('name', 'email', 'created_at')
	actions = ["export_as_csv"]

@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
	list_display = ('name', 'email', 'created_at')

@admin.register(IdeaCategory)
class IdeaCategoryAdmin(admin.ModelAdmin):
	list_display = ('name',)

@admin.register(ServiceSocial)
class ServiceSocialAdmin(admin.ModelAdmin):
	list_display = ('name', 'email', 'career', 'created_at')