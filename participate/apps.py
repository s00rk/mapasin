from django.apps import AppConfig


class ParticipateConfig(AppConfig):
    name = 'participate'
    verbose_name = 'Participa'