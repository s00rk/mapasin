# Generated by Django 2.0.2 on 2018-03-28 23:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('galery', '0007_auto_20180319_2305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Category', verbose_name='Biblioteca'),
        ),
    ]
