from django.db import models

from library.models import Category, ProjectType

class Photo(models.Model):
	title = models.CharField(max_length=200, verbose_name='Titulo')
	file = models.ImageField(upload_to='galery/', verbose_name='Foto')
	category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Biblioteca')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Foto'
		verbose_name_plural = 'Fotos'

	def __str__(self):
		return self.title

class Video(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	url = models.CharField(max_length=200, blank=True, verbose_name='Youtube ID (watch?v=YoutubeID)')
	file = models.FileField(upload_to='video/', blank=True, verbose_name='Video')
	category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Biblioteca')

	class Meta:
		verbose_name = 'Video'
		verbose_name_plural = 'Videos'

	def __str__(self):
		return self.name
