"""mapasin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from django.views.generic import TemplateView

from app import views
from participate import views as ev

urlpatterns = [
	path('', views.IndexView.as_view()),
	path('galeria', views.GaleryView.as_view()),
	path('contacto', views.ContactView.as_view()),
	path('blog', views.BlogView.as_view()),
    path('blog/<str:slug>', views.ArticleView.as_view()),
    path('proyecto/<str:slug>', views.ProjectView.as_view()),
    path('proyecto/<str:slug>/<str:project_type>', views.ProjectTypeView.as_view()),
    path('objetivos', views.ObjectiveView.as_view()),
    path('aviso-privacidad', views.PrivacityView.as_view()),
    path('plan-conecta', views.PlanConnectView.as_view()),
    path('plan-conecta-info', views.PlanConnectInfoView.as_view()),
    path('plan-avanza', views.PlanAdvanceView.as_view()),
    path('plan-avanza/objetivo', views.PlanAdvanceObjectiveView.as_view()),
    path('proximamente', views.CommingSoonView.as_view()),
    path('buscar', views.SearchView.as_view()),

    path('participa', views.ParticipateView.as_view()),
    path('participa/eventos', ev.EventsView.as_view()),
    path('participa/evento/<str:slug>', ev.EventView.as_view()),
    path('participa/<str:type>', views.ParticipateView.as_view()),

    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
