from django.shortcuts import render, redirect
from django.template.response import TemplateResponse 
from django.views.generic import TemplateView
from django.core.paginator import Paginator

from app.models import App, Alliance
from participate.models import Subscriber, ServiceSocial, IdeaCategory, Idea
from galery.models import Photo, Video
from blog.models import Article
from library.models import Category, Project, ProjectType, Map

def getCategories():
	return Category.objects.order_by('order')

class IndexView(TemplateView):
	template_name = 'index.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class ContactView(TemplateView):
	template_name = 'contact.html'

	def get(self, request, *args, **kwargs):
		alliances = Alliance.objects.all()
		return render(request, self.template_name, { 'categories': getCategories(), 'alliances': alliances })

class GaleryView(TemplateView):
	template_name = 'galery.html'

	def get(self, request, *args, **kwargs):
		photos = Photo.objects.order_by('-created_at')
		return render(request, self.template_name, { 'photos': photos, 'categories': getCategories() })

class ParticipateView(TemplateView):
	template_name = 'participate.html'

	def getTemplate (self, type=None):
		template_name = self.template_name
		if type == 'servicio-social':
			template_name = 'social_service.html'
		elif type == 'informacion':
			template_name = 'information.html'
		elif type == 'ideas':
			template_name = 'idea.html'
		elif type == 'donativos':
			template_name = 'donate.html'
		return template_name

	def get(self, request, type=None, *args, **kwargs):
		data = { 'categories': getCategories() }
		if type == 'ideas':
			data['ideacategories'] = IdeaCategory.objects.order_by('name')
		elif type == 'informacion':
			data['posts'] = Article.objects.order_by('-created_at')[:3]

		return render(request, self.getTemplate(type), data)

	def post(self, request, type, *args, **kwargs):
		data = request.POST

		if type == 'servicio-social':
			if not 'name' in data or not 'email' in data or not 'career' in data or not 'message' in data:
				return redirect(request.get_full_path())

			servicesocial = ServiceSocial()
			servicesocial.name = data['name']
			servicesocial.email = data['email']
			servicesocial.career = data['career']
			servicesocial.work = data['message']
			servicesocial.save()

		elif type == 'informacion':
			if not 'name' in data or not 'email' in data:
				return redirect(request.get_full_path())

			subscriber = Subscriber()
			subscriber.name = data['name']
			subscriber.email = data['email']
			subscriber.save()

		elif type == 'ideas':
			if not 'name' in data or not 'email' in data or not 'message' in data:
				return redirect(request.get_full_path())

			idea = Idea()
			idea.name = data['name']
			idea.email = data['email']
			idea.idea = data['message']
			if 'file' in request.FILES:
				idea.file = request.FILES['file']
			if 'category' in data:
				idea.category = IdeaCategory.objects.filter(pk=data['category']).first()
			idea.save()


		return render(request, self.getTemplate(type), { 'categories': getCategories(), 'success': True })

class BlogView(TemplateView):
	template_name = 'blog_list.html'

	def get(self, request, *args, **kwargs):
		paginator = Paginator(Article.objects.order_by('-created_at'), 25)

		page = request.GET.get('page')
		articles = paginator.get_page(page)

		current = articles.number

		pages = []
		for p in paginator.page_range:
			if p == (current-2) or p == (current-1) or p == current or p == (current+1) or p == (current+2):
				pages.append(p)
		return render(request, self.template_name, { 'posts': articles, 'categories': getCategories(), 'pages': pages })

class ArticleView(TemplateView):
	template_name = 'blog_article.html'

	def get(self, request, slug, *args, **kwargs):
		post = Article.objects.filter(slug=slug).first()

		return render(request, self.template_name, { 'post': post, 'categories': getCategories() })

class ProjectView(TemplateView):
	template_name = 'types.html'

	def get(self, request, slug, *args, **kwargs):
		types = ProjectType.objects.order_by('id')

		return render(request, self.template_name, { 'types': types, 'categories': getCategories(), 'slug': slug })

class ProjectTypeView(TemplateView):
	template_name = 'project.html'

	def get(self, request, slug, project_type, *args, **kwargs):


		title = ProjectType.objects.filter(name=project_type).first().name
		response = {
			'slug': slug,
			'categories': getCategories(),
			'type': title
		}

		nameType = project_type.lower()

		if nameType == 'fotos':
			photos = Photo.objects.filter(category__slug=slug).order_by('-id')
			response['photos'] = photos
			return render(request, 'photos.html', response)
		elif nameType == 'videos':
			videos = Video.objects.filter(category__slug=slug).order_by('-id')
			response['videos'] = videos
			return render(request, 'videos.html', response)
		elif nameType == 'mapas':
			maps = Map.objects.filter(category__slug=slug).order_by('-id')
			response['maps'] = maps
			return render(request, 'maps.html', response)

		projects = Project.objects.filter(category__slug=slug, projectType__name=project_type)

		response['projects'] = projects
		return render(request, self.template_name, response)

class ObjectiveView(TemplateView):
	template_name = 'objective.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class PrivacityView(TemplateView):
	template_name = 'privacity.html'

	def get(self, request, *args, **kwargs):
		privacity = ''

		app = App.objects.first()
		if app is not None:
			privacity = app.privacity

		return render(request, self.template_name, { 'privacity': privacity, 'categories': getCategories() })

class PlanConnectView(TemplateView):
	template_name = 'plan_connect.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class PlanConnectInfoView(TemplateView):
	template_name = 'plan_connect_info.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class PlanAdvanceView(TemplateView):
	template_name = 'plan_advance.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class PlanAdvanceObjectiveView(TemplateView):
	template_name = 'plan_advance_objective.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class CommingSoonView(TemplateView):
	template_name = 'coming_soon.html'

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, { 'categories': getCategories() })

class SearchView(TemplateView):
	template_name = 'search.html'

	def get(self, request, *args, **kwargs):

		response = []
		search = request.GET.get('search')

		if search is None:
			return render(request, self.template_name, { 'categories': getCategories(), 'search': response, 'data': search })

		objects = Project.objects.filter(projectType__name='INVESTIGACIONES', title__icontains=search).order_by('-id')[:200]
		if objects.count() > 0:
			response.append({ 'slug': 'INVESTIGACIONES', 'data': objects })

		objects = Project.objects.filter(projectType__name='PLANES Y PROYECTOS', title__icontains=search).order_by('-id')[:200]
		if objects.count() > 0:
			response.append({ 'slug': 'PLANES Y PROYECTOS', 'data': objects })

		objects = Photo.objects.filter(title__icontains=search).order_by('-id')[:200]
		if objects.count() > 0:
			response.append({ 'slug': 'FOTOS', 'data': objects })

		objects = Video.objects.filter(name__icontains=search).order_by('-id')[:200]
		if objects.count() > 0:
			response.append({ 'slug': 'VIDEOS', 'data': objects })

		objects = Map.objects.filter(title__icontains=search).order_by('-id')[:200]
		if objects.count() > 0:
			response.append({ 'slug': 'MAPAS', 'data': objects })

		return render(request, self.template_name, { 'categories': getCategories(), 'search': response, 'data': search })
