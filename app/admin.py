from django import forms
from django.contrib import admin
from django.db import models

from .models import App, Alliance

@admin.register(App)
class AppAdmin(admin.ModelAdmin):
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

	class Media:
		js = ('ckeditor/ckeditor.js', 'ckeditor/margin.js')

@admin.register(Alliance)
class AllianceAdmin(admin.ModelAdmin):
	list_display = ('name', 'url', 'created_at', 'updated_at')