from django.db import models

class App(models.Model):
	privacity = models.TextField(verbose_name='Aviso de privacidad')

	class Meta:
		verbose_name = 'App'
		verbose_name_plural = 'App'

class Alliance(models.Model):
	name = models.CharField(max_length=200, verbose_name='Nombre')
	url = models.URLField(max_length=200, verbose_name='Url')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Alianza'
		verbose_name_plural = 'Alianzas'

	def __str__(self):
		return self.name
