from django.contrib import admin

from .models import Plan, PlanFile, File

@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
	list_display = ('name', 'created_at')

@admin.register(PlanFile)
class PlanFileAdmin(admin.ModelAdmin):
	list_display = ('plan', 'title', 'created_at')

@admin.register(File)
class FileAdmin(admin.ModelAdmin):
	list_display = ('planfile', 'file', 'created_at')