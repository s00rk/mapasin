from django.db import models

class Plan(models.Model):
	name = models.CharField(max_length=100, verbose_name='Nombre')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Plan'
		verbose_name_plural = 'Planes'

	def __str__(self):
		return self.name

class PlanFile(models.Model):
	plan = models.ForeignKey(Plan, on_delete=models.CASCADE, verbose_name='Plan')
	title = models.CharField(max_length=300, verbose_name='Titulo')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Plan Documento'
		verbose_name_plural = 'Plan Documento'

	def __str__(self):
		return '%s - %s' % (self.category, self.title)

class File(models.Model):
	planfile = models.ForeignKey(PlanFile, on_delete=models.CASCADE, verbose_name='Plan Documento')
	file = models.FileField(upload_to='plan/', verbose_name='PDF')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Documento'
		verbose_name_plural = 'Documentos'

	def __str__(self):
		return '%s - %s' % (self.planfile, self.file)