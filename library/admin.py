from django.contrib import admin

from .models import Category, Project, ProjectType, Map

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'order')

#@admin.register(ProjectType)
class ProjectAdmin(admin.ModelAdmin):
	list_display = ('name',)

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
	list_display = ('category', 'title', 'projectType', 'pdf')

@admin.register(Map)
class MapAdmin(admin.ModelAdmin):
	list_display = ('category', 'title', 'url')
