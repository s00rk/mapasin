from django.db import models
from django.utils.text import slugify
import itertools

class Category(models.Model):
	name = models.CharField(max_length=100, verbose_name='Nombre')
	order = models.PositiveIntegerField(default=0)
	slug = models.SlugField(max_length=300, unique=True, editable=False)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Categoria'
		verbose_name_plural = 'Categorias'

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.name)[:300]
			for x in itertools.count(1):
				if not Category.objects.filter(slug=self.slug).exists():
					break
				self.slug = "%s-%d" % (self.name[:300 - len(str(x)) - 1], x)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.name

class ProjectType(models.Model):
	name = models.CharField(max_length=100, verbose_name='Nombre')
	image = models.ImageField(upload_to='library_type', verbose_name='Imagen')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Tipo'
		verbose_name_plural = 'Tipos'

	def __str__(self):
		return self.name


class Project(models.Model):
	category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Categoria')
	title = models.CharField(max_length=300, verbose_name='Titulo')
	projectType = models.ForeignKey(ProjectType, on_delete=models.CASCADE, verbose_name='Tipo')
	pdf = models.FileField(upload_to='library/', verbose_name='PDF')
	photo = models.FileField(upload_to='library_photo/', verbose_name='Foto')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Proyecto / Investigación'
		verbose_name_plural = 'Proyectos / Investigaciones'

	def __str__(self):
		return '%s - %s' % (self.category, self.title)

class Map(models.Model):
	category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Categoria')
	title = models.CharField(max_length=300, verbose_name='Titulo')
	photo = models.FileField(upload_to='library_map/', verbose_name='Imagen')
	url = models.URLField(verbose_name='Url')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Mapa'
		verbose_name_plural = 'Mapas'

	def __str__(self):
		return '%s - %s' % (self.category, self.title)