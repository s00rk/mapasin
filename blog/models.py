from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
import itertools

class Article(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	title = models.CharField(max_length=200, verbose_name='Titulo')
	photo = models.ImageField(upload_to='articles/', verbose_name='Foto')
	slug = models.SlugField(max_length=300, unique=True, editable=False)
	content = models.TextField(verbose_name='Contenido')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'Articulo'
		verbose_name_plural = 'Articulos'

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)[:300]
			for x in itertools.count(1):
				if not Article.objects.filter(slug=self.slug).exists():
					break
				self.slug = "%s-%d" % (self.title[:300 - len(str(x)) - 1], x)
		super().save(*args, **kwargs)

	def __str__(self):
		return self.title


class Comment(models.Model):
	article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name='Articulo')
	name = models.CharField(max_length=100, verbose_name='Nombre')
	email = models.EmailField(verbose_name='Email')
	content = models.TextField(verbose_name='Contenido')
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Comentario'
		verbose_name_plural = 'Comentarios'

	def __str__(self):
		return self.article
