from django import forms
from django.db import models
from django.contrib import admin
from .models import Article, Comment

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
	list_display = ('title', 'user', 'created_at', 'updated_at')
	exclude = ['slug']
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

	def save_model(self, request, obj, form, change):
		if not obj.user.id:
			obj.user = request.user
		obj.save()

	class Media:
		js = ('ckeditor/ckeditor.js', 'ckeditor/margin.js')

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
	list_display = ('article', 'name', 'created_at')